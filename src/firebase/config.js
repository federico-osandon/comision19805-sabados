import { initializeApp } from "firebase/app";

const firebaseConfig = {
  apiKey: "AIzaSyD1Hnhea-e3nkmhgyTYPdhlKbG3N5Gs7ps",
  authDomain: "comision19805.firebaseapp.com",
  projectId: "comision19805",
  storageBucket: "comision19805.appspot.com",
  messagingSenderId: "179820561739",
  appId: "1:179820561739:web:d8a9c1de6fdb707ed7d4ce"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);

export default function getFirestoreApp() {
    return app
}