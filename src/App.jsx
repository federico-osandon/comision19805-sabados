import { BrowserRouter, Routes, Route } from 'react-router-dom'
import NavBar from './components/NavBar/NavBar'
import ItemListContainer from './components/ItemListContainer'
import './App.css'
import 'bootstrap/dist/css/bootstrap.min.css';
import Cart from './components/Cart/Cart';
import ItemDetailContainer from './components/ItemDetailContainer/ItemDetailContainer';
import CartContextProvider from './context/CartContext';

export default function App () {  
    // estados nuestros      
    //console.log(cartContext)
    //estados y funciones globales

    return (
        <BrowserRouter>
            <CartContextProvider>
                <div className="App border border-3 border-primary" 
                    //onClick={()=> alert('soy evento del app')} 
                >    
                    <NavBar /> 
                    <Routes>                    
                        { /* esto es la vinculación ... */ }
                        <Route exact path='/' element={ <ItemListContainer />} />
                        <Route exact path='/categoria/:idCategoria' element={ <ItemListContainer />} />
                        <Route exact path='/detalle/:idProducto' element={ <ItemDetailContainer />} />
                        <Route exact path='/cart' element={ <Cart />} />                  
                    </Routes>              
                </div>      
            </CartContextProvider>
        </BrowserRouter>
    )
}


