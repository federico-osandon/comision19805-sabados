import React from 'react'
import ReactDOM from 'react-dom'
import './index.css'
import  App from './App'
import getFirestoreApp from './firebase/config'

getFirestoreApp()

// App() == <App />

ReactDOM.render( <App /> ,  document.getElementById('root'))
