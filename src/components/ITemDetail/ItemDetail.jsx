

import { useState } from "react";
import { Link } from "react-router-dom";
import { useCartContext } from "../../context/CartContext";
import ItemCount from "../ItemCount.js/ItemCount";

const ItemDetail = ({product}) => {
    const [contador, setContador] = useState(0)
    const {  agregarAlCarrito } = useCartContext()

    function onAdd(cant) {
        agregarAlCarrito( { item: product , cantidad: cant } )
        setContador( cant)
    }

  return <div>
        <div className="row">
            <div className="col">
                <div className="container">
                    <h3>{product.name}</h3>
                    <img src={product.imagenUrl} alt='Foto'/>
                </div>
            </div>
            <div className="col">
                {
                    contador === 0 ?
                        <ItemCount onAdd={onAdd} initial={1} stock={6} />                    
                    : 
                        <>
                            <Link to='/cart'>
                                <button>Terminar compra</button>
                            </Link>
                            <Link to='/'>
                                <button>Seguir Comprando</button>
                            </Link>
                        </>
                }
            
            </div>
        </div>

  </div>;
};

export default ItemDetail;
