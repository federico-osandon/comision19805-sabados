import { doc, getDoc, getFirestore } from 'firebase/firestore';
import { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import { getFetch } from '../../helpers/mock';
import ItemDetail from '../ITemDetail/ItemDetail';

const ItemDetailContainer = () => {
    const [product, setProduct] = useState({});
    const [loading, setLoading] = useState(true);
    
    const { idProducto } = useParams()

    useEffect(() => {
        //llamada a una api. Tarea asincónica  
        const db = getFirestore()      
        const itemRef = doc(db, 'items', idProducto) 
        getDoc(itemRef)
        .then(resp => setProduct( { id: resp.id, ...resp.data() } ))
        .catch(err => console.log(err))
            //.then(respuesta => console.log(respuesta))
        .finally(()=> setLoading(false))               
       
        //console.log('api')     
    }, [])

    console.log(product)
    
    return (
        <>  

            { loading ? 
                <h2>Cargando ...</h2>
            :             
                <ItemDetail product={product} />
            }
        </>
        );
};

export default ItemDetailContainer;
