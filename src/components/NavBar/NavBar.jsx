import { useCartContext } from '../../context/CartContext'
import { Link, NavLink } from "react-router-dom"

import  Container  from "react-bootstrap/Container"
import  Navbar  from "react-bootstrap/Navbar"
import  Nav  from "react-bootstrap/Nav"
import  NavDropdown  from "react-bootstrap/NavDropdown"
import Cartwidget from "../CartWiidget/Cartwidget"

import './NavBar.css'


const NavBar = () => {

    const { cantidad } = useCartContext()    
    
    return (
        <>
            <Navbar collapseOnSelect expand="lg" bg="dark" variant="dark">
                <Container >
                    <NavLink to='/' className={ ({isActive}) => isActive ? 'boton' : ''}>
                        React-Ecommerce
                    </NavLink>                   
                    <Navbar.Toggle aria-controls="responsive-navbar-nav" />
                    <Navbar.Collapse id="responsive-navbar-nav">
                        <Nav className="me-auto">
                            <NavLink to='/categoria/gorras' className={ ({isActive}) => isActive ? 'boton' : ''} >Gorras</NavLink>
                            <NavLink to='/categoria/remeras' className={ ({isActive}) => isActive ? 'boton' : ''} >Remeras</NavLink>                   
                        </Nav>
                    </Navbar.Collapse>
                </Container>
                <Nav>
                    {/* <Nav.Link href="#deets">More deets</Nav.Link> */}
                    <Link to='/cart'>
                        { cantidad() !== 0 && cantidad()}
                        <Cartwidget />                        
                    </Link>                          
                </Nav>
            </Navbar>
            
        </>
    )
}

export default NavBar
