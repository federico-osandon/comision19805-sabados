import React, { memo } from 'react';
import { Items } from '../Item/Item';



// memo(fn(componente))    -    memo(fn, fn que evlua) 
const ItemList = memo( ( {productos} ) => {
                            console.log('itemlist')
                          return <>
                                { productos.map( prod => <Items prod={prod}  /> )}
                            </>;
                          }
                        , (oldProp, newProp)=> oldProp.productos.length === newProp.productos.length )

export default ItemList;
