import imag from './carrito/carrito.jpg'
import { BsCart } from "react-icons/bs";


const Cartwidget = () => {
    
    return (
        <div className='w-25'>
            <img src="assets/imagen/carrito.jpg" alt="carrito" className='w-25'/>
        </div>
    )
}

export default Cartwidget
