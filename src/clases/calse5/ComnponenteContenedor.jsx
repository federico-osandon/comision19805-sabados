import { useState, useEffect } from 'react'
import Input from "./Input"
import { Items } from "./Item"
import NavBar from "./NavBar/NavBar"
import Titulo from "./Titulo"


function ComnponenteContenedor() {
    const [count, setCount] = useState( 0 )
    const [bool, setBool] = useState(false)
    
    
    //let count = 0
   
   const obj = {nombre: 'fede', ape: 'osandon'} 
   
   const handlerCount = () => {
        setCount( count+1 )  // count++ // count = count +1        
   }

    useEffect(()=>{  // siempre despues de cada render          
            console.log('despues de renderizado 1')//adEvnetListener
            // return ()=>{
            //     console.log('limpieza')//removeEventListe
            // }
    })

    useEffect(()=>{            // solo una vez después de la primer carga
            console.log('Llamada a api, carga asincrónica, o función pesada 2')
    }, [])

    useEffect(()=>{            // solo una vez después de la primer carga
            console.log('cambio de estado booleano 3')
            //setBool(!bool)
    }, [bool])

   console.log('componente montado 4')
   console.log(bool)

    return (
        <div>   
            <Titulo persona={obj} />        
            <Items /> 
             {count}<br/>
            
            <button onClick={ handlerCount }>Aumentar</button>
            <button onClick={ ()=> setBool(!bool) }>CAmbiar booleano</button>
        </div>
    )
}

export default ComnponenteContenedor
