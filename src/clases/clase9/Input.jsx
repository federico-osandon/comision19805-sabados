import {useState, useEffect} from 'react'
// import './input.css'


export const Input = () => {
    const [data, setData] = useState({ letra: '' });

    const inputHandler = (event)=>{
        //event.stopPropagation()
        console.log(event.key)
       
    }
    console.log(data);
    
    return (
        <div className="box" >
            <div className="border border-1 border-warning" >
                <input 
                    name='letra'
                    className="m-5" 
                    onKeyDown={inputHandler} 
                    type="text"                     
                />
            </div>
        </div>
    )
}
