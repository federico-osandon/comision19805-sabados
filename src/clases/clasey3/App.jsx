
import './App.css'
import Titulo from './components/Titulo'


function Items() {
    return(
        <div>
             <Titulo tit='Soy Titulo del componente Item' subTitulo='Subtitulo de Titulo de Item' />
        </div>
        
    )
    
}

export default function App () {  

  // const condition = false
  //let resultado = null

  // if (condition) {
  //   resultado = 'verdadero'
  // } else {
  //   resultado = 'falso'
  // }

  // console.log('el resultado es:'+resultado)

  // condicion ? : ,  condition && accion, condition || acciones
  // console.log(`El resultado es: ${condition ? 'verdadero' : 'falso'}`)

  // let a = 'a'
  // const array = ['b','c','d']
  
  // const newArray = [...array, a]


  // const id= 'lkajsdflasjfdsad'

  // const obj = {
  //   nombre: 'fede',
  //   edad: 35,
  //   ['apellido'+id]: 'Osandon'blue
  // }
  // const obj = {
  //   nombre: 'fede',
  //   apellido: 'osandon',    
  //   edad:30
  // }


  // let nombre =  obj.nombre
  // let apellido = obj.apellido


// destructuring
  // const { nombre: primerNombre, edad=35  } = obj

  // console.log(edad)

  
    const estilo = { backgroundColor: 'blue' }
    const tit = 'Fede el mejor'
    const subTitulo='Es verdad'
    // <Titulo tit='fede el mejor' /> == Titulo({tit:'fede el mejor'})

    return (
        <div className="App" >     
           <Titulo tit={tit} subTitulo={subTitulo} />
           <Titulo tit='Soy Titulo del componente Titulo' subTitulo='Subtitulo de Titulo' />
           <Items />
            <p>Fede</p>            
            <input />
        </div>
    )
}


